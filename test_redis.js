const assert = require('assert')
const redis = require('redis')
const argv = require('minimist')(process.argv.slice(2))
assert(argv.host)
assert(argv.port)

const redis_param = {
    host:argv.host,
    port:argv.port,
    detect_buffers: true
}

const pub = redis.createClient(redis_param)
    , sub = redis.createClient(redis_param)

sub.on('message', function (channel, message) {
    console.log('got message', channel, message)
})

sub.subscribe("chat")