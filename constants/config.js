module.exports = exports = {
    PORT: process.env.CHAT_LISTEN_PORT || 30001,

    CHAT: {
        REDIS_IP: process.env.CHAT_REDIS_IP || '127.0.0.1',
        REDIS_PORT: process.env.CHAT_REDIS_PORT || 6379,
    }
}
