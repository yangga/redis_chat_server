### What is this repository for? ###

Very simple chat server using redis. It is scale-outable designed. Fork this for creating a new chat server.

### How do I get set up? ###

npm install

export CHAT_REDIS_IP=127.0.0.1
export CHAT_REDIS_PORT=6379

### Contribution guidelines ###

Contact : yangga0070@gmail.com

### Who do I talk to? ###

Contact : yangga0070@gmail.com