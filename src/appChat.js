const assert = require('assert')
const socketio = require('socket.io')
const redis = require('redis')
const config = require('constants/config')

function Chat(httpServer, props) {
    assert(props.channels && props.channels instanceof Array)

    const self = this

    self.connectedCnt = 0;
    self.channels = props.channels

    self.io = self.initSocket(httpServer)
    self.initRedis(props.channels)
}

Chat.prototype.initRedis = function(channels) {
    const self = this

    const redis_param = {
        host:config.CHAT.REDIS_IP,
        port:config.CHAT.REDIS_PORT,
        detect_buffers: true
    }

    self.__pub = redis.createClient(redis_param) 
    const sub = redis.createClient(redis_param)

    sub.on('message', self._onSubMessage.bind(self))

    channels.map((c) => sub.subscribe(c))
}

Chat.prototype._onSubMessage = function(channel, message) {
    console.log('got message', channel, message)
    this.io.to(channel).emit(channel, message)
}

Chat.prototype.initSocket = function(httpServer) {
    const self = this

    const io = socketio(httpServer);
    
    io.on('connection', (socket) => {
        socket.on('disconnect', function() {
            --self.connectedCnt;
            console.log('Client disconnect, left: ', self.connectedCnt);
        });

        socket.on('joinChannel', function(channelName) {
            console.log('joined channel', channelName)
            socket.join(channelName)
            socket.emit('joinedChannel', channelName)
        })

        socket.on('emit', function(channelName, message) {
            self.sendToChannel(channelName, message)
        })

        socket.join('joined')

        self.connectedCnt++
        console.log('Client connect - left: ', self.connectedCnt);
    })

    return io
}

Chat.prototype.sendToAll = function(message) {
    this.__pub.publish('joined', message)
}

Chat.prototype.sendToChannel = function(channelName, message) {
    this.__pub.publish(channelName, message)
}

module.exports = exports = Chat
